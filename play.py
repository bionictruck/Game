
print "Welcome to the game!"

name = raw_input('Please enter a name: ')

print 'Hello ' + name + ', you are standing in a large room with the other recruits.'
print """"Attention!", screams the lady apparently in charge. "I am General Klyla. You have been selected to see if \
any of you have what it takes to become full members of the Order of the Hawk." She stared at a few recruits before \
continuing. "After you complete your training, you will be able to sell your services identifying yourself as a member \
of the Order of the Hawk. Members of the Order demand a higher price for their services and are much more likely to be \
hired."""